<?php

use Illuminate\Database\Seeder;
use App\Item;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        Category::create(
            [
                'name' => 'Phones',
                'description' => 'Small Electronic Devices'
            ]
        ); 
        Category::create(
            [
                'name' => 'House Electronic Devices',
                'description' => 'Big Electronic Devices'
            ]
        ); 
        Category::create(
            [
                'name' => 'Furniture',
                'description' => 'Furniture'
            ]
        ); 

        Item::create(
            [
                'name' => 'Samsung S8',
                'category_id' => 1,
                'description' => 'Year of production : 2018'
            ]
        );
        Item::create(
            [
                'name' => 'Samsung S7',
                'category_id' => 1,
                'description' => 'Year of production : 2017'
            ]
        );
        Item::create(
            [
                'name' => 'Samsung S6',
                'category_id' => 1,
                'description' => 'Year of production : 2016'
            ]
        );

        Item::create(
            [
                'name' => 'iPhone 7',
                'category_id' => 1,
                'description' => 'Year of production : 2016'
            ]
        );

        Item::create(
            [
                'name' => 'Hover',
                'category_id' => 2,
                'description' => 'Big Electronic Devices'
            ]
        );


        Item::create(
            [
                'name' => 'Double Bed',
                'category_id' => 3,
                'description' => 'part of a set'
            ]
        );

    }
}
// $or = new Order();
// $or->person = "Kaftan";
// $or->employee  = "Ciyaar Ciyaar";

// $orderItem = new OrderItem();
// $orderItem2 = new OrderItem();
// $orderItem2 = new OrderItem();
// $orderItem3 = new OrderItem();
// $orderItem->item_id = 4;
// $orderItem2->item_id = 5;
// $orderItem3->item_id = 6;
// $orderItem->price = 650;
// $orderItem2->price = 500;
// $orderItem3->price = 120;
// $orderItem->quantity = 10000;
// $orderItem3->quantity = 8000;
// $orderItem2->quantity = 6000;
// $or->addOrder(new Sale(),[$orderItem,$orderItem2,$orderItem3]);