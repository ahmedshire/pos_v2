<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

//purchases
Route::group(['prefix' => 'purchases'], function(){
    Route::get('/', 'OrdersController@purchases')->name('purchases');
    Route::get('/new', function(){return view('orders.purchases.create');})->name('new_purchase');
    Route::post('/new', 'OrdersController@create')->name('new_purchase');
    Route::delete('/{id}', 'OrdersController@destroy')->name('delete_purchase');
});// end of purchases' routes

Route::group(['prefix' => 'sales'], function(){
    Route::get('/', 'OrdersController@sales')->name('sales');
    Route::get('/new', function(){return view('orders.sales.create');})->name('new_sale');
    Route::post('/new', 'OrdersController@create')->name('new_sale');    
    Route::delete('/{id}', 'OrdersController@destroy')->name('delete_sale');    
});// end of sales' routes


Route::group(['prefix' => 'refunds'], function(){
    Route::get('/','OrdersController@refunds')->name('refunds');
    Route::get('/new', function(){return view('orders.refunds.create');})->name('new_refund');
    Route::post('/new', 'OrdersController@create')->name('new_refund');    
    Route::delete('/{id}', 'OrdersController@destroy')->name('delete_refund');  
});// end of refunds' routes



Route::group(['prefix' => 'inventory'], function(){
    Route::get('/', 'ItemsController@index')->name('inventory');
    
    Route::get('/new', 'ItemsController@manipulate')->name('new_item');
    Route::post('/new', 'ItemsController@manipulate')->name('new_item');

    Route::get('/{id}',  'ItemsController@manipulate')->name('edit_item');
    Route::post('/{id}', 'ItemsController@manipulate')->name('edit_item');

    Route::delete('/{id}', 'ItemsController@destroy')->name('delete_item');
});// end of inventory routes

Route::group(['prefix' => 'categories'], function(){
    Route::get('/', 'ItemsController@categories')->name('categories');
    Route::post('/','ItemsController@categories')->name('categories');
    Route::get('/{id}','ItemsController@categories')->name('edit_category');
    Route::post('/{id}','ItemsController@categories')->name('edit_category');
    Route::delete('/{id}','ItemsController@categories')->name('delete_category');
});


Route::group(['prefix' => 'reports'], function(){
    Route::group(['prefix' => 'inventory'], function(){
        Route::get('/','InventoryReportsController@index')->name('inventory_reports');
        Route::get('/export','InventoryReportsController@export')->name('export_inventory_report');
        Route::post('/search_inventory','InventoryReportsController@search')->name('search_inventory');
        Route::get('/{items}','InventoryReportsController@single')->name('single_item_report');
        
    });
    //purchases
    Route::group(['prefix' => 'purchases'], function(){
        Route::get('/', 'OrdersReportsController@purchases')->name('report_purchases');
        Route::get('/export','OrdersReportsController@exportPurchases')->name('export_purchases_report');
        Route::post('/', 'OrdersReportsController@search')->name('report_purchases');
        Route::get('/{id}', 'OrdersReportsController@single')->name('single_order_export');
    });// end of purchases' routes

    Route::group(['prefix' => 'sales'], function(){
        Route::get('/', 'OrdersReportsController@sales')->name('report_sales');
        Route::get('/export','OrdersReportsController@exportSales')->name('export_sales_report');
        Route::post('/', 'OrdersReportsController@search')->name('report_sales');
        Route::get('/{id}', 'OrdersReportsController@single')->name('single_order_export');   
    });// end of sales' routes


    Route::group(['prefix' => 'refunds'], function(){
        Route::get('/', 'OrdersReportsController@refunds')->name('report_refunds');
        Route::get('/export','OrdersReportsController@exportRefunds')->name('export_refunds_report');
        Route::post('/', 'OrdersReportsController@search')->name('report_refunds');
        Route::get('/{id}', 'OrdersReportsController@single')->name('single_order_export');
    });// end of refunds' routes

});
