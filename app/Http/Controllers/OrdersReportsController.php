<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderItem;
use App\Purchase;
use App\Sale;
use App\Refund;

use Illuminate\Http\Request;

class OrdersReportsController extends Controller
{

    # Contructor
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    # show purchases index page
    public function purchases()
    {
        $orders = $this->order->has('purchase')->get();
        $data['orders'] = $orders;
        $data['title'] = "Purchase";
        $data['personTitle'] = "Supplier";
        session()->put('to_report_purchase', $orders);

        return view('reports.orders.purchases.index')->with($data);


    }

    # show sales index page
    public function sales()
    {
        $orders = $this->order->has('sale')->get();
        
        $data['orders'] = $orders;
        $data['title'] = "Sale";
        $data['personTitle'] = "Customer";

        session()->put('to_report_sale', $orders);
        return view('reports.orders.sales.index')->with($data);


    }

    # show refunds index page
    public function refunds()
    {
        $orders = $this->order->has('refund')->get();
        
        $data['orders'] = $orders;
        $data['title'] = "Refund";
        $data['personTitle'] = "Customer";

        session()->put('to_report_refund', $orders);
        return view('reports.orders.refunds.index')->with($data);


    }

    public function search(Request $request)
    {
        return redirect(route(report_purchases));
    }

    # export a single order anda its items xlsx
    public function single($id)
    {
       $order = $this->order->find($id);

        if (!$order){
            $feedback['errors'] = [' Failed to generate report, please try again later.'] ;               
        }else{
            $filename = ($order->getType() == "App\\Purchase") ? 'Purchase' :  (
                $order->getType() == "App\\Sale" ? 'Sale' : (
                    $order->getType() == "App\\Refund" ? 'Refund' : 'Unknown'
                ) 
            );
            
            $filename .= "_report";
            header("Content-Disposition: attachment; filename=$filename.xlsx");
            return view('reports.orders.export_single_order')->with('order', $order);
        }
    }


    

    # export a xlsx file reading the content from session
    public function exportPurchases(Request $request)
    {
        $orders = [];
        $filename = "purchase";
        if ($request->session()->has('to_report_purchase')){
            $orders = $request->session()->pull('to_report_purchase');

            if($request->session()->has('purchase_report_filename'))
                $filename = $request->session()->pull('purchase_report_filename');        
            if(count($orders) > 0){
                // dd($orders);
                
                header("Content-Disposition: attachment; filename=$filename.xlsx");
                return view('reports.orders.export_orders')->with('orders', $orders);

            }else{
                $feedback['infos'] = ['Operation Apported. This is an empty list of orders.'] ;
                session()->put('feedback', $feedback);
                return redirect(route('report_purchases'));
            }

        }else{
            $feedback['errors'] = [' Failed to generate report, please try again later.'] ;
            session()->put('feedback', $feedback);
            return redirect(route('report_purchases'));            
        }
    }


    # export a xlsx file reading the content from session
    public function exportSales(Request $request)
    {
        $orders = [];
        $filename = "sale";

        if ($request->session()->has('to_report_sale')){
            $orders = $request->session()->pull('to_report_sale');

            if($request->session()->has('sale_report_filename'))
                $filename = $request->session()->pull('sale_report_filename');        
            if(count($orders) > 0){
                header("Content-Disposition: attachment; filename=$filename.xlsx");
                return view('reports.orders.export_orders')->with('orders',$orders);

            }else{
                $feedback['infos'] = ['Operation Apported. This is an empty list of orders.'] ;
                session()->put('feedback', $feedback);
                return redirect(route('report_sales'));
            }

        }else{
            $feedback['errors'] = [' Failed to generate report, please try again later.'] ;
            session()->put('feedback', $feedback);
            return redirect(route('report_sales'));            
        } 
    }

    # export a xlsx file reading the content from session
    public function exportRefunds(Request $request)
    {
        $orders = [];
        $filename = "refund";
        
        if ($request->session()->has('to_report_refund')){
            $orders = $request->session()->pull('to_report_refund');

            if($request->session()->has('refund_report_filename'))
                $filename = $request->session()->pull('refund_report_filename');        
            if(count($orders) > 0){
                header("Content-Disposition: attachment; filename=$filename.xlsx");
                return view('reports.orders.export_orders')->with('orders',$orders);

            }else{
                $feedback['infos'] = ['Operation Apported. This is an empty list of orders.'] ;
                session()->put('feedback', $feedback);
                return redirect(route('report_refunds'));
            }

        }else{
            $feedback['errors'] = [' Failed to generate report, please try again later.'] ;
            session()->put('feedback', $feedback);
            return redirect(route('report_refunds'));            
        }
    }

}
