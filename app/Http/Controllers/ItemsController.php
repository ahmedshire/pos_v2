<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Item;

class ItemsController extends Controller
{
    public function __construct(Item $item)
    {
        $this->item = $item;
        $this->category = new Category();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['items'] = \App\Item::all();
        return view('inventory.index')->with($data);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['item']   = $this->item;
        $data['action']  = 'new_item';
        $data['title']  = 'New Item';
        return view('inventory.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = false;

        # Retrieving Request data
        $this->item                 = new Item();
        $this->item->name           = $request['name'];
        $this->item->category_id    = $request['category_id'];
        $this->item->description    = $request['description'];
//        $this->item->sellingPrice   = $request['sellingPrice'];
        
        
        // # Validate

        // # save to db
        $success = $this->item->save();
        $err_msg = $success ? " Record Successfullt added to the database." : "  problem occured, please try again later.";
        session()->put('feedback', ['errors' => $err_msg  ]);

        return  redirect(route(($success ? 'inventory' : 'new_item' )));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function manipulate(Request $request, $id=0)
    {

        //
        $err_msg = "";
        $success_msg ="";
        $success = false;
        $this->item = $this->item->find($id);

        // check if desired item exists in the database , if not then redirect back with message 
        if(!$this->item && $id != 0){
            // prepare error message
            $feedback['errors'] = ["This item doesn't exist in database"];
            // add error message to session
            session()->put('feedback', $feedback);
            // reditrect to inventory page 
            return redirect(route('inventory'));
        }else{

            $this->item =$id ? $this->item : new Item();
            $data['item'] = $this->item;
            
            $data['action'] = $id ? 'edit_item': 'new_item';
            $data['title'] = $id ? 'Edit Item' . $this->item->name : 'New Item';
            
            $data['categories'] = $this->category->all();
            
            if ($request->isMethod('get')) {
                return view('inventory.create')->with($data);


            }elseif($request->isMethod('post')){

                # getting post data
                $this->item->name = $request['name'];
                $this->item->category_id = $request['category_id'];
                $this->item->description = $request['description'];

                # Validating Data

                # saving to db
                $success = $this->item->save();
                $err_msg = "Operation Failed ! try again later.";
                $success_msg = "Operation Successful ! Item " . $this->item->name . " was successfully edited";
                
            }

            $feedback[($success ? 'successs' : 'errors')] = $success ? [$success_msg] : [$err_msg];
            $route = $success ? redirect(route('inventory')) :  view('inventory.create')->with($data);

            session()->put('feedback', $feedback);
            
            return $route;
        }
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $success = false;
        $this->item = $this->item->find($id);

        # if item has a dependants redirect back to categories list
        if($this->item->orderItems()->count()){
            $err_msg = "Can't delete item, delete item's dependancies first"; 
            
        }else{    
            # Selecting the record from database 
            $success = $this->item->delete(); 
            $err_msg = $success ? "Operation successful, Record deleted" : "Operation failed";
            
        }
        
        if(!$success){
            $feedback['errors'] = [$err_msg] ;
        }else
            $feedback['successs'] = [$err_msg] ;

        #redirect after success
        session()->put('feedback', $feedback);
        return redirect(route('inventory')); 

    }
    public function categories(Request $request, $id=0)
    {

        /**
         * 
         * ONE ACTION FOR ALL THE CRUD OPERATIONS 
         * 
         */
        $success = false;
         //prepare data for create page get method
        $date = [];
        if ($id == 0) {
            # when id is not set then its a new category
            $data['pageTitle'] = 'New Category ';
            $data['category'] = $this->category;


        }else{
            # id is set then its a modify 
            // first check if the record exists in the database and redirect if not

            $this->category = $this->category->find($id);
            if ($this->category == null) {
                $feedback['errors'] = ["Operation Failed ! Category doesn't exist in database."] ;
                session()->put('feedback', $feedback);
                return redirect(route('categories'));
            }
            $data['pageTitle'] = 'Edit Category ' . $this->category->name;
            $data['category'] = $this->category;

        }
        $data['categories'] = $this->category->all();


        if ($request->isMethod('get')){ 
            return view('inventory.categories', $data);

        }elseif ($request->isMethod('post')) {
            # get POST data
           
            $this->category->name           = $request->input('name');
            $this->category->description    = $request->input('description');

            # validate POST data
            $this->validate($request,['name'    => 'required',]);
            
            # Insert data to db
            $success = $this->category->save();

            if(!$success){
                $feedback['errors'] = [" Operation Failed, Please try again later."] ;

            }else
                $feedback['successs'] = ["Operation Successful ! Category " . $this->category->name . " was successfully added to database."] ;

            #redirect after success
            session()->put('feedback', $feedback);
            return redirect(route('categories'));       
                 
        }elseif ($request->isMethod('delete')) {

            # if category has a dependants redirect back to categories list
            if($this->category->items()->count()){
                $err_msg = "Can't delete category, delete category's dependancies first"; 
                
            }else{    
                # Selecting the record from database 
                $success = $this->category->delete(); 
                $err_msg = $success ? "Operation successful, Record deleted" : "Operation failed";
                
            }
            
            if(!$success){
                $feedback['errors'] = [$err_msg] ;
            }else
                $feedback['successs'] = [$err_msg] ;

            #redirect after success
            session()->put('feedback', $feedback);
            return redirect(route('categories')); 
        }
    }
}
