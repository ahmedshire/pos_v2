<?php

namespace App\Http\Controllers;

use App\Item;
use App\Category;

use Illuminate\Http\Request;

class InventoryReportsController extends Controller
{

    # Contructor
    public function __construct(Item $item)
    {
        $this->item = $item;
        $this->category = new Category();
    }

    # show index page if invevntory 
    public function index()
    {
        $items = $this->item->all();
        $data['items'] = $items;
        session()->put('to_report_inventory', $items);
        return view('reports.inventory.index')->with($data);
    }

    public function search(Request $request)
    {
        # First we check which form is sending the post
        $form = isset($request['category_id']) ? "category" :( 
            isset($request['price_from']) && isset($request['price_to']) ? "priceRange" : (
                 isset($request['cost_from']) && isset($request['cost_to']) ? "cost" : null
            )
        );

        $data = [];
        $feedback = null;


        switch ($form) {
            case 'category':
                $category_id = $request['category_id'];
                $category = Category::find($category_id);
                if ($category == null){
                    $feedback['errors'] = ["This Category doesn't exists in the database."];
                    $items = $this->item->all();
                    break;
                }
                $items = $category->items;

                if (count($items) == 0) 
                    $feedback['infos'] = ["No items for the category " . $category->name];

                break;
            case 'priceRange':
                $price_from = $request['price_from'];
                $price_to = $request['price_to'] ? $request['price_to'] : 0;
                $items = $this->item->byPriceRange($price_from, $price_to);
                if (count($items) == 0) 
                    $feedback['infos'] = ["No items in the wanted range : from $price_from ands $price_to "] ;
                break;
            case 'costRange':
                $cost_from = $request['cost_from'];
                $cost_to = $request['cost_to'] ? $request['cost_to'] : 0;
                $items = $this->item->byCostRange($cost_from, $cost_to);
                if (count($items) == 0) 
                    $feedback['infos'] = ["No items in the wanted range : from $cost_from ands $cost_to "] ;

                break;
            default:
                $items = $this->item->all();
                break;
        }
        session()->put('to_report_inventory', $items);
        $data['items'] = $items;
        if($feedback)
            session()->put('feedback', $feedback);
                $data['items'] = $items;
        return view('reports.inventory.index')->with($data);
    }

    public function single($id)
    {
        # check if item exists
        $this->item = $this->item->find($id);
        if (!$this->item) {
            $feedback['errors'] = ["this items doesn't exist in the database."] ;
            session()->put('feedback', $feedback);
            return redirect()->back();
        }

        header("Content-Disposition: attachment; filename=item_detailed_report.xlsx");
        return view('reports.inventory.export_items')->with('items',[$this->item]);


        
    }


    # export a csv file reading the content from session
    public function export(Request $request)
    {
        $items = [];
        $filename = "inventory";
        
        if ($request->session()->has('to_report_inventory')){
            $items = $request->session()->pull('to_report_inventory');

            if($request->session()->has('inventory_report_filename'))
                $filename = $request->session()->pull('inventory_report_filename');        
            
            if(count($items) > 0){
                // $handle = fopen($filename, 'w+');
                // fputcsv($handle, array('Name', 'Category',
                //  'Unit Cost', 'Unit Price',
                // 'In Stock', 'Purchased Quantity', 'Sold Quantity', 
                // 'Returned Quantity', 'Profit', 'Sale Income' ,'Total Refunds' ));

                // foreach($items as $item) {
                //     fputcsv(
                //         $handle, 
                //         array(
                //             $item->name ,
                //             $item->category->name , 
                //             ($item->getAvgCost() == 0 ? " Not Set" : number_format($item->getAvgCost() , 2, '.', '')), 
                //             ($item->getAvgPrice() == 0 ? " Not Set" : number_format($item->getAvgPrice() , 2, '.', '')), 
                //             $item->inStock(),
                //             $item->quantityPurchased() ,            
                //             $item->quantitySold() ,        
                //             $item->quantityReturned(),
                //             $item->profit(),
                //             $item->getTotalSaleIncome(),
                //             $item->getTotalRefunds()
                //         )
                //     );
                // }


                // #fputcsv($handle, array('', '',
                // # '', '',
                // #'', '', '', 
                // #'', '', '', '' ));                
                // fclose($handle);
                // $headers = array('Content-Type' => 'text/csv' );             
                // return response()->download($filename, $filename, $headers)->deleteFileAfterSend(true);;
                header("Content-Disposition: attachment; filename=$filename.xlsx");
                return view('reports.inventory.export_items')->with('items',$items);

            }else{
                $feedback['errors'] = ['Operation Apported. Empty List of items.'] ;
                session()->put('feedback', $feedback);
                return redirect(route('inventory_reports'));
            }

        }else{
            $feedback['errors'] = [' Failed to generate report, please try again later.'] ;
            session()->put('feedback', $feedback);
            return redirect(route('inventory_reports'));            
        }
    }

}
