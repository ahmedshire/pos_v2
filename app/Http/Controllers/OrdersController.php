<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderItem;
use App\Purchase;
use App\Sale;
use App\Refund;
use App\Item;

class OrdersController extends Controller
{
    /**
     * 
     * ORDER TYPES FROM FORM POST ARE:
     *      - 0 FOR PURCHASE
     *      - 1 FOR SALE
     *      - 2 FOR REFUND
     * 
     * Declaring feedback array in the construction
     * 
     */

    public function __construct(Order $order)
    {
        $feedback = [];
        $this->order = $order;
    }

    public function purchases()
    {
        $data['orders'] = \App\Order::has('purchase')->get();
        $data['title'] = "Purchase";
        $data['personTitle'] = "Supplier";
        $data['new_route'] = "new_purchase";
        $data['edit_route'] = "edit_purchase";
        $data['delete_route'] = "delete_purchase";
        
        return view('orders.purchases.index')->with($data);


    }

    public function sales()
    {
        $data['orders'] = \App\Order::has('sale')->get();
        $data['title'] = "Sale";
        $data['personTitle'] = "Supplier";
        $data['new_route'] = "new_sale";
        $data['edit_route'] = "edit_sale";
        $data['delete_route'] = "delete_sale";
        
        return view('orders.sales.index')->with($data);


    }

    public function refunds()
    {
        $data['orders'] = \App\Order::has('refund')->get();
        $data['title'] = "Refund";
        $data['personTitle'] = "Supplier";
        $data['new_route'] = "new_refund";
        $data['edit_route'] = "edit_refund";
        $data['delete_route'] = "delete_refund";
        
        return view('orders.refunds.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // for both GET and SET methods for all order types

        if($request->isMethod('post')){
            $order_types = 
            [
                [
                    get_class(new Purchase()), 'purchase'
                ],
                [
                    get_class(new Sale()), 'sale'
                ],
                [
                    get_class(new Refund()), 'refund'
                ]
            ];

            $order_type_index = $request['order_type'];
            if(isset($order_type_index)){
                $order_type = new $order_types[$order_type_index][0]();
                $relation = $order_types[$order_type_index][1];
            }else{
                session()->put('feedback', array('errors' => ["order_type is not set please chose wisely"]));
                return back();
            }

            $order_items_post_arrays = [];
            $order_items_post_arrays['item_id'] = $request['item_id'];
            $order_items_post_arrays['price'] = $request['price'];
            $order_items_post_arrays['quantity'] = $request['quantity'];
            
            $orderItems = [];
            foreach ($order_items_post_arrays['item_id'] as $index => $item_id) {
                
                $orderItem = new OrderItem(
                    [
                        'item_id' => $order_items_post_arrays['item_id'][$index] ,
                        'price'=> $order_items_post_arrays['price'][$index] ,
                        'quantity'=> $order_items_post_arrays['quantity'][$index]                         
                    ]
                );

                array_push($orderItems,$orderItem);
            }
            if (!isset($orderItems)) {
                session()->put('feedback', array('errors' => ["You cant add a $relation without items. "]));
                return back();                
            }

            $order = new Order();
            $order->person = $request['person'];
            $order->employee = $request['employee'];
            $order->description = $request['description'];
            $order->created_at = $request['created_at'];
            
            $success = $order->saveOrder($order_type, $orderItems);
            if(!$success){
                $feedback['errors'] = [$order->feedback ? $order->feedback : " Operation Failed, Please try again later."] ;
                session()->put('feedback', $feedback);
                $data['order'] = $order;
                $data['orderItems'] = $orderItems;
                session()->put('data', $data);
                
                // dd($data);
                // dd(session()->get('feedback'));
                return redirect()->back()->withInput();
                // return view("orders.{$relation}s.create" ,$data);
            }    
            $feedback['successs'] = "Operation Successful ! $relation : " . $order->$relation->serial . " was added to the database." ;
            return redirect(route($relation.'s'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = $this->order->find($id);
        // dd($order);
        if(!$order){
            $feedback['errors'] = [" Operation Failed, This record doesn't exist in the database."] ;
            session()->put('feedback', $feedback);
            return redirect()->back();
            
        }
        $serial = $order->relation()->serial;
        $success = $order->remove();
        if(!$success){
            $feedback['errors'] = [$order->feedback ? $order->feedback : " Operation Failed, Please try again later."] ;
        }else
            $feedback['successs'] = ["Operation Successful ! Order # " . $serial . " was successfully removed from the database."] ;

        session()->put('feedback', $feedback);
        return redirect()->back();
    }
}
