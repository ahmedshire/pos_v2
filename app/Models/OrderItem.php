<?php


namespace App;

use Illuminate\Database\Eloquent\Model;


class OrderItem extends Model
{

	public $timestamps = false;

	protected $fillable = [
		'order_id',
		'item_id',
		'price',
		'quantity',
		'type',
		'amount'
	];

	public function item()
	{
		return $this->belongsTo(\App\Item::class);
	}

	public function order()
	{
		return $this->belongsTo(\App\Order::class);
	}

	# get amount
	public function getAmount()
	{
		return $this->quantity * $this->price;
	}
	
	public function saveItem(){
		$this->amount = $this->getAmount();
		return $this->save();
	}
}