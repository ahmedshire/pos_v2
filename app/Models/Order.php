<?php


namespace App;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{
	public $feedback = "";

	protected $fillable = [
		'person',
		'employee',
		'description'
	];

	public function orderItems()
	{
		# return all items for this order
		return $this->hasMany(\App\OrderItem::class, 'order_id', 'id');
	}

	public function amount()
	{
		return $this->orderItems->sum('amount');
	}
	
	# get order type : purchase , sale or refund
	public function getType()
	{
		$orderTypes = [
			$this->purchase,
			$this->sale,
			$this->refund
		];
		foreach ($orderTypes as $type) 
			if($type !== null)
				return get_class($type);
		return 0;
		
	}

	# get inistance of one to one relation: purchase, sale or refund
	public function relation()
	{
		return $this->purchase ? $this->purchase : ($this->sale ? $this->sale : ($this->refund ? $this->refund : false) );
	}
	
	public function purchase()
	{
		return $this->hasOne(\App\Purchase::class, 'id');
	}

	public function refund()
	{
		return $this->hasOne(\App\Refund::class, 'id');
	}

	public function sale()
	{
		return $this->hasOne(\App\Sale::class, 'id');
	}

	/**
	 * 
	 * Add Items to Order
	 * 
	 */

	# add a single item
	public function addItem(OrderItem $orderItem) 
	{
		$orderItem->order_id = $this->id;
		$orderItem->type = $this->getType();
		if(!$orderItem->type)
			return false;
		
		try{
			return $orderItem->saveItem();
		}catch(Exception $exception){
			return false;
		}
	}

	# add array of Items
	public function addItems($orderItems)
	{
		if (!$orderItems)
			return false;

		// dd($orderItems);


		$item_ids = [];
		foreach ($orderItems as $orderItem) {
			array_push($item_ids, $orderItem->item_id );
		}
		if (count($orderItems) > count(array_unique($item_ids)) ) {
			$this->feedback = " you can't add the same item more than once in a single order.";
			return false
			;
		}

		foreach ($orderItems as $orderItem) 
			if(!$this->addItem($orderItem))
				return false;

		return true;
		
	}


	# generate unique order number for tables: purchases, sales and refunds
	public function generateOrderSerial($order_type)
	{

		$order_class_name = get_class($order_type);
		//
		$data = [
			Purchase::class 	=> [
				'prefix' 	=> 'PO-',
			],
			Sale::class 	=> [
				'prefix' 	=> 'SO-',
			],
			Refund::class 	=> [
				'prefix' 	=> 'RO-',
			]		
		];

		if(isset($data[$order_class_name])){
			# get the desired prefix and relation from the array
			$prefix = $data[$order_class_name]['prefix'];

			
			# get the last order of the desired type : Purchase, Sale or Refund
			$orders_of_type = $order_type->orderBy('id','desc')->get();
			$last_serial = $orders_of_type->count() ? $orders_of_type->first()->serial:  $prefix."00000";;
				 

			try{
				$x = explode('-', $last_serial)[1];
				return $prefix. str_repeat("0", (5 - strlen($x+1))) . ($x+1); 
			}catch(Exception $ex){
				return false;
			}
		}
		return false;
	}

	/**
	 * 
	 * 
	 * adding orders and orderitems
	 * 
	 * 
	 */

	public function saveOrder($order_type, $orderItems){
		# 	order_relation OBJECT, indicates what type of order: Purchase, Sale or Refund

		if (!$order_type || !count($orderItems)) 
			return false;
		
		
		$order_class_name = get_class($order_type);
		# 	saving the order to orders table and getting the id
		if(!$this->save())
			return false ;
		
		# 	creating the order type: purchase, sale or refund and assign the order id to it and saving to the db
		$order_type->id = $this->id;
		$order_type->serial = $this->generateOrderSerial($order_type);

		try{
			$success = $order_type->save();
		}catch(Exception $ex){
			$success = false;
		}
		
		# 	adding the order items to the id 
		if (!$success) {
			$this->remove();
			return false;
		}

		$success = $this->addItems($orderItems);

		# 	return true of success or remove the saved records if false 

		if (!$success) {
			$this->feedback = $this->feedback ? $this->feedback : " try again later.";
			$this->remove();
			return false;
		}

		# return boolean success
		return $success;
	}



	# delete order if depenecies are deleted 
	public function remove()
	{
		$success = false;
		
		# delete on to one relation
		$success = $this->removeAssociate();

		# delete on to many sub table relation
		if ($success) 
			$success = $this->removeOrderItems();
		
		# delete order 
		if ($success) 
			$success = $this->delete();

		# return boolean 
		return $success;
	}
	# delete one to one relations to the order
	public function removeAssociate(){
		$class_name = $this->getType();
		if(!$class_name)
			return true;
		$relation = strtolower(explode('\\', $class_name)[1]);
		$success = false;
		try{
			if (!$relation) 
				return false;
			$orderAssociate = $this->$relation;
			$success = $orderAssociate->delete();
		}catch(Exception $exception){
			return false;
		}
		return $success;
	}

	# delete order's items 
	public function removeOrderItems()
	{
		$orderItems = $this->orderItems;
		$success = false;

		if (!$orderItems->count())
			return true;
		
		foreach ($orderItems as $orderItem) 
			$success = $orderItem->delete();

		return $success;
	}




	public function DELETE_ALL_ORDERS(){
		// ONLY FOR DEVELOPMENT PURPOSES
		$orders = $this->all();;
		$success = false;
		foreach ($orders as $order) {
			$success = $order->remove();
		}

		return $success;
	}


}


