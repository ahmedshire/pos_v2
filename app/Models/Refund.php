<?php


namespace App;

use Illuminate\Database\Eloquent\Model;


class Refund extends Model
{

	public $timestamps = false;

	protected $fillable = [
		'id',
		'serial'
	];
	
	public function order()
	{
		return $this->belongsTo(\App\Order::class, 'id');
	}
}
