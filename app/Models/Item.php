<?php


namespace App;

use Illuminate\Database\Eloquent\Model;


class Item extends Model
{
	public $timestamps = false;

	protected $fillable = [
		'name',
		'category_id',
		'description',
		'sellingPrice'
	];

	public function category()
	{
		return $this->belongsTo(\App\Category::class);
	}

	public function orderItems()
	{
		# return all record ordred from this item either sale or purchase 
		return $this->hasMany(\App\OrderItem::class);
	}

	# get all order made for this item : purchases, sales and refunds
	public function orders()
	{
		return $this->orderItems->pluck('order');
	}	

	#get List of Items by Category
	public function byCategory($category_id)
	{
		return $this->where('category_id', $category_id)->get();
	}

	#get List of Items by Category
	public function byCostRange($from = 0, $to)
	{
		$items = $this->all();
		foreach ($items as $index => $item) {
			if ($item->getAvgCost() < $from || $item->getAvgCost() > $to) 
				unset($items[$index]);
		}
		return $items;
	}

	#get List of Items by price range
	public function byPriceRange($from, $to)
	{
		$items = $this->all();
		foreach ($items as $index => $item) {
			if ($item->getAvgPrice() < $from || $item->getAvgPrice() > $to) 
				unset($items[$index]);
		}
		return $items;
	}


	#get List of profitable Items 
	public function profitable()
	{
		$items = $this->all();
		foreach ($items as $index => $item) {
			if($item->profit() >= 0)
				$items->pull($index);
		}
		return $items;
	}


	public static function availableItems()
	{
		$availableItems =  Item::all();
		foreach ($availableItems as $index => $item) 
			if ($item->inStock() <= 0) 
					$availableItems->pull($index);

		return $availableItems;

	}

	public static function soldItems()
	{
		$soldItems =  Item::all();
		foreach ($soldItems as $index => $item) 
			if ($item->quantityActualSold() <= 0) 
					$soldItems->pop($index);
		
		return $soldItems;

	}

	# get this items orders by type : purchase, sale or refund
	public function getOrderByType(string $order_type)
	{
		$orders = $this->orders();
		return $orders->where($order_type ,'<>', null); 
	}

	# get this items purchase orders
	public function purchaseOrders()
	{
		return $this->getOrderByType('purchase');
	}

	# get this items sale orders
	public function saleOrders()
	{
		return $this->getOrderByType('sale');
	}
	
	# get this items refund orders
	public function refundOrders()
	{
		return $this->getOrderByType('refund');
	}	

	// get orderItems by order type : parameter object of Order Type : new Purchase(), new Sale() or new Refund()
	public function getOrderItemsByType($order_type)
	{
		return $this->orderItems->where('type',get_class($order_type));
	}

	/**
	 * 
	 * Calculations
	 * 
	 */

	// Quantities

	# get total quantity of a given type of this items' order
	private function getQuantitiy($order_type)
	{
		return $this->getOrderItemsByType($order_type)->sum('quantity');
	}

	# get total purchased quantity
	public function quantityPurchased()
	{
		return $this->getQuantitiy(new Purchase());
	}

	# get total sold quantity
	public function quantitySold()
	{
		return $this->getQuantitiy(new Sale());
	}
	# get total returned quantity
	public function quantityReturned()
	{
		return $this->getQuantitiy(new Refund());
	}

	# get actual Sold quantity
	public function quantityActualSold()
	{
		return $this->quantitySold() - $this->quantityReturned() ;
	}

	# get available in stock quantity
	public function inStock()
	{
		return $this->quantityPurchased() - $this->quantitySold() + $this->quantityReturned() ;
	}

	// money
	
	# get total ammount of a given type of this items' order
	private function getAmount($order_type)
	{
		return $this->getOrderItemsByType($order_type)->sum('amount');
	}
	# get total purchase cost
	public function getTotalPurchaseCost()
	{
		return $this->getAmount(new Purchase());
	}

	# get Avg. purchase Cost 
	public function getAvgCost()
	{
		$ratio = $this->quantityPurchased() ? 1 / $this->quantityPurchased() : 0; 
		return $this->getTotalPurchaseCost() * $ratio;
	}

	# get total sale incomes of this item 
	public function getTotalSaleIncome()
	{
		return $this->getAmount(new Sale());
	}
	# get Avg. sale price  
	public function getAvgPrice()
	{
		$ratio = $this->quantitySold() ? 1 / $this->quantitySold() : 0; 
		return $this->getTotalSaleIncome() * $ratio;
	}
	# get total refund ammount  
	public function getTotalRefunds()
	{
		return $this->getAmount(new Refund());
	}
	# get Avg refund price
	public function getAvgRefund()
	{
		$ratio = $this->quantityReturned() ? 1 / $this->quantityReturned() : 0; 
		return $this->getTotalRefunds() * $ratio;
	}

	public function profit()
	{
		return ($this->getTotalSaleIncome() -  $this->getTotalRefunds()) - ($this->quantityActualSold() * $this->getAvgCost());
	}
}


