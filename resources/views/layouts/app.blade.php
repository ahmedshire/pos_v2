<!DOCTYPE html>
<html class="no-js">
    
    <head>
        <title>{{ config('app.name') }}</title>
        <!-- Bootstrap -->
        <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">
        <link href="{{ asset('bootstrap/css/bootstrap-responsive.min.css') }}" rel="stylesheet" media="screen">
        <link href="{{ asset('vendors/easypiechart/jquery.easy-pie-chart.css') }}" rel="stylesheet" media="screen">
        <link href="{{ asset('assets/styles.css') }}" rel="stylesheet" media="screen">
        <link href="{{ asset('assets/DT_bootstrap.css') }}" rel="stylesheet" media="screen">
        <link href="{{ asset('vendors/datepicker.css') }}" rel="stylesheet" media="screen">
        <link href="{{ asset('vendors/chosen.min.css') }}" rel="stylesheet" media="screen">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="{{ asset('vendors/modernizr-2.6.2-respond-1.1.0.min.js') }}"></script>
        <style>
            select:focus {
                width: auto;
                position: relative;
            }
        .chzn-select{ overflow:visible; }
        /* .ui-dialog{ overflow:visible; } */

        </style>
    </head>
    
    <body>
        <script src="{{ asset('vendors/jquery-1.9.1.js') }}"></script>
        <script src="{{ asset('vendors/jquery-1.9.1.min.js') }}"></script>
        <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
        
        <script src="{{ asset('vendors/datatables/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/DT_bootstrap.js') }}"></script>

        <script src="{{ asset('vendors/easypiechart/jquery.easy-pie-chart.js') }}"></script>
        <script src="{{ asset('assets/scripts.js') }}"></script>
        <script src="{{ asset('vendors/chosen.jquery.min.js') }}"></script>
        <script src="{{ asset('vendors/bootstrap-datepicker.js') }}"></script>
        <script src="{{ asset('vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('assets/form-validation.js') }}"></script>
        <section id="nav_top" >
            @include('layouts.navs.top') 
        </section>
        <div class="container-fluid">
            
            <div class="row-fluid">
                <section id="nav_sidebar" >
                    @include('layouts.navs.sidebar')
                </section>
                <div class="span9" id="content">

                    <div class="row-fluid">

                       
                        <section id="nav_inpage">

                            <div class="navbar">
                                <div class="navbar-inner">
                                    <ul class="breadcrumb">
                                        <i class="icon-chevron-left hide-sidebar">
                                            <a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a>
                                        </i>
                                        <i class="icon-chevron-right show-sidebar" style="display:none;">
                                            <a title="Show Sidebar" rel='tooltip'>&nbsp;</a>
                                        </i>

                                        <li class="{{ Request::is('/') ? 'active' : ''}}">     
                                            <a href="{{ route('home') }}">Dashboard</a>
                                        </li>
                                        @php($segments = '')
                                        @foreach(Request::segments() as $segment)
                                            @php($segments .= '/'.$segment)
                                            <li>
                                                <span class="divider">/</span>
                                                <a href="{{ $segments }}">{{ is_numeric($segment) ? 'Edit' : ucfirst($segment) }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </section>
                            
                    </div>                    
                    @yield('content')
                </div>
            </div>
            <hr>
            <footer class="pull-right">
                <p>&copy; Tabaarak ICT Solutions </p>
            </footer>
        </div>
        <!--/.fluid-container-->
        


        <script>
        // $(function() {
            // Easy pie charts
            // $('.chart').easyPieChart({animate: 1000});
        // });
        jQuery(document).ready(function() {   
            FormValidation.init();
        });
        $(function() {
            $(".datepicker").datepicker();
            $(".chzn-select").chosen();
        });
        </script>
    </body>

</html>