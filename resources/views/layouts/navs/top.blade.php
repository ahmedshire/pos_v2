<div class="navbar navbar-fixed-top">

    <div class="navbar-inner">
    
        <div class="container-fluid">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </a>
            <a class="brand" >{{ config('app.name') }}</a>
            
            <div class="nav-collapse collapse">

                <!-- <ul class="nav pull-right">
                    <li class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> Vincent Gabriel <i class="caret"></i>

                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a tabindex="-1" href="#">Profile</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a tabindex="-1" href="login.html">Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul> -->

                <ul class="nav">
                    <li class="{{ Request::is('/') ? 'active' : ''}}">     
                        <a href="{{ route('home') }}">Dashboard</a>
                    </li>
                    <li class="dropdown">
                        <a  data-toggle="dropdown" class="dropdown-toggle">Orders<b class="caret"></b></a>
                        <ul class="dropdown-menu sub-menu">
                            <li>
                                <a href="{{ route('purchases') }}">Purchase Orders</a>
                            </li>
                            <li>
                                <a href="{{ route('sales') }}">Sale Orders</a>
                            </li>
                            <li>
                                <a href="{{ route('refunds') }}">Refunds</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a  data-toggle="dropdown" class="dropdown-toggle">Reports<b class="caret"></b></a>
                        <ul class="dropdown-menu sub-menu">
                            <li>
                                <a href="{{ route('inventory_reports') }}">Inventory Reports</a>
                            </li>
                            <li>
                                <a href="{{ route('report_purchases') }}">Purchases Reports</a>
                            </li>
                            <li>
                                <a href="{{ route('report_sales') }}">Sales Reports</a>
                            </li>
                            <li>
                                <a href="{{ route('report_refunds') }}">Refunds Reports</a>
                            </li>                                                        
                        </ul>
                    </li>

                    
                    <!-- <li class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Content <i class="caret"></i>

                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a tabindex="-1" href="#">Blog</a>
                            </li>
                            <li>
                                <a tabindex="-1" href="#">News</a>
                            </li>
                            <li>
                                <a tabindex="-1" href="#">Custom Pages</a>
                            </li>
                            <li>
                                <a tabindex="-1" href="#">Calendar</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a tabindex="-1" href="#">FAQ</a>
                            </li>
                        </ul>
                    </li> -->
                    <!-- <li class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Users <i class="caret"></i>

                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a tabindex="-1" href="#">User List</a>
                            </li>
                            <li>
                                <a tabindex="-1" href="#">Search</a>
                            </li>
                            <li>
                                <a tabindex="-1" href="#">Permissions</a>
                            </li>
                        </ul>
                    </li> -->
                </ul>

            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>