<div class="span3" id="sidebar">
    
    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">

        <li class="{{ Request::is('/') ? 'active' : ''}}">     
            <a href="{{route('home')}}"><i class="icon-chevron-right"></i> Dashboard</a>
        </li>
        <li class="{{ Request::is('purchases') ? 'active' : ''}}">
            <a href="{{ route('purchases') }}"><span class="badge badge-{{ App\Purchase::all()->count() ? 'success' : 'primary' }} pull-right ">{{ App\Purchase::all()->count() }}</span> Purchase Orders</a>
        </li>
        <li class="{{ Request::is('sales') ? 'active' : ''}}">        
            <a href="{{ route('sales') }}"><span class="badge badge-{{ App\Sale::all()->count() ? 'success' : 'error' }} pull-right">{{ App\Sale::all()->count() }}</span> Sale Orders</a>
        </li>
        <li class="{{ Request::is('refunds') ? 'active' : ''}}">        
            <a href="{{ route('refunds') }}"><span class="badge badge-{{ App\Refund::all()->count() ? 'success' : 'primary' }} pull-right">{{ App\Refund::all()->count() }}</span> Refunds</a>
        </li>
        
        <li class="{{ Request::is('inventory') ? 'active' : ''}}">
            <a href="{{ route('inventory') }}"><span class="badge badge-{{ App\Item::all()->count() ? 'success' : 'warning' }} pull-right">{{ App\Item::all()->count() }}</span> Inventory</a>
        </li>
    </ul>
</div>

<!--/span-->