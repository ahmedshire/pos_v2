@if($feedback = session()->pull('feedback'))

    @if(isset($feedback['errors']))
    @php($errors = $feedback['errors'])
    <!-- FOR ERROR MESSAGES -->
    <div class="alert alert-error">
        <button class="close" data-dismiss="alert">&times;</button>
        @foreach($errors as $error)
            <strong>Error!</strong> {{ $error }}.
            <br/>
        @endforeach

    </div>
    @endif


    @if(isset($feedback['successs']))
    @php($successs = $feedback['successs'])
    <!-- FOR SUCCESS MESSAGES -->
    <div class="alert alert-success">
        <button class="close" data-dismiss="alert">&times;</button>
        
        @foreach($successs as $success)
            <strong>Success!</strong> {{ $success }}.
            <br/>
        @endforeach

    </div>
    @endif

    @if(isset($feedback['infos']))
    @php($infos = $feedback['infos'])
    <!-- FOR INFORMATION -->
    <div class="alert alert-info">
        <button class="close" data-dismiss="alert">&times;</button>
        
        @foreach($infos as $info)
            <strong>Info!</strong> {{ $info }}.
            <br/>
        @endforeach

    </div>
    @endif


    @if(isset($feedback['warnings']))
    @php($warnings = $feedback['warnings'])
    <div class="alert">
        <button class="close" data-dismiss="alert">&times;</button>
        @foreach($warnings as $warning)
        <strong>warning!</strong> {{ $warning }}.
        <br/>
        @endforeach

    </div>
    @endif
@endif
