<h1>Order # :  {{$order->relation()->serial}}</h1>
<h4>{{($order->getType() == "App\\Purchase") ? 'Supplier' :  'Customer'}} Name : {{$order->person}}</h4>
<h4>Employee :  {{$order->employee}}</h4>


<h3>Order Items</h3>

<table>
    <thead>
        <tr>
            <th>#</th>
            <th> Item </th>
            <th> Price </th>
            <th> Quantity </th>
            <th> Amount </th>
        </tr>    	
    </thead>
    <tbody>
        @foreach($order->orderItems as $index => $orderItem)
        <tr>
            <td>{{ $index + 1 }}</td>
            <td>{{ $orderItem->item->name }} </td>
            <td>{{ $orderItem->price }} </td>
            <td>{{ $orderItem->quantity }} </td>
            <td>{{ $orderItem->amount }} </th>
        </tr> 
        @endforeach   	

    </tbody>

    <tfoot>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th> Total : {{ $order->amount() }} </th>
        </tr>
    </tfoot>

</table>