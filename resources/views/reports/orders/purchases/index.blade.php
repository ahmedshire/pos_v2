@php
$is_report =1;
$categories = \App\Category::all();
$single_order_report = 'single_purchase_report';
$export_route = 'export_purchases_report';
@endphp

@extends('layouts.app')

@section('content')
@include('includes.order_reports')
    
@endsection