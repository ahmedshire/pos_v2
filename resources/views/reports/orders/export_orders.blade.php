<table>
<thead>
    <tr>
        <th>#</th>
        <th>Order No#</th>
        <th>{{($orders->first()->getType() == "App\\Purchase") ? 'Supplier' :  'Customer'}}</th>
        <th>Employee</th>
        <th>Total Amount</th>
        <th>Number of Items</th>
    </tr>
</thead>
<tbody>
    @foreach($orders as $index => $order)
    <tr class="">
        <td>{{ $index + 1}}</td>
        <td>{{ $order->relation() ? $order->relation()->serial : '' }}</td>
        <td>{{ $order->person }}</td>
        <td>{{ $order->employee }}</td>
        <td class="center"> ${{ $order->amount() }}</td>
        <td class="center"> <b>{{ $order->orderItems->count() }}</b> items </td>
    </tr>
    @endforeach
</tbody>