@php
$is_report =1;
$categories = \App\Category::all();
$single_order_report = 'single_sale_report';
$export_route = 'export_sales_report';
@endphp

@extends('layouts.app')

@section('content')
@include('includes.order_reports') 
@endsection