@php
$is_report =2;
$categories = \App\Category::all();
$single_order_report = 'single_refund_report';
$export_route = 'export_refunds_report';
@endphp

@extends('layouts.app')

@section('content')
@include('includes.order_reports')  
@endsection