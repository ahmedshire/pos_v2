
<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example2">
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Category</th>
            <th>Unit Cost</th>
            <th>Unit Price</th>
            <th>Description</th>
            <th>In Stock</th>
            <th>Purchased Quantity</th>
            <th>Sold Quantity</th>
            <th>Returned Quantity</th>
            <th>Profit</th>
            <th>Sale Income</th>
            <th>Total Refunds</th>
           
        </tr>
    </thead>
        @foreach($items as $index => $item)
        <tr class="">
            <td>{{ $index + 1  }}</td>
            <td>{{ $item->name  }}</td>
            <td>{{ $item->category->name  }}</td>
            <td>{{ ($item->getAvgCost() == 0 ? " Not Set" : number_format($item->getAvgCost() , 2, '.', ''))  }}</td>
            <td>{{ ($item->getAvgPrice() == 0 ? " Not Set" : number_format($item->getAvgPrice() , 2, '.', ''))  }}</td>
            <td>{{ $item->description }}</td>
            <td>{{ $item->inStock() }}</td>
            <td>{{ $item->quantityPurchased() }}</td>
            <td>{{ $item->quantitySold() }}</td>
            <td>{{ $item->quantityReturned() }}</td>
            <td>{{ $item->profit() }}</td>
            <td>{{ $item->getTotalSaleIncome() }}</td>
            <td>{{ $item->getTotalRefunds() }}</td>
        </tr>
        @endforeach
</table>
