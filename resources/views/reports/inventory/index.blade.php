@php
$is_report =1;

$categories = \App\Category::all();

@endphp

@extends('layouts.app')

@section('content')

<div class="block">
    <div class="navbar navbar-inner block-header">
        <a id="add" class="btn btn-success btn-mini pull-right " data-toggle="collapse" data-target="#search_forms" class="top-bar-right"> 
            <b>Search Forms</b>
        </a>
    </div>
    <div id="">
        <fieldset>



            <div id="search_forms" class="block-content collapse ">

                <div class="navbar navbar-inner block-header">

                    <div class="muted pull-left">Search Forms</div>


                </div>

                <div class="block-content  ">

                    <div class="span12">

                        <table class="table span12">

                            
                            <tbody class="detail">


                                <div class="div_cat">
                                    
                                    <tr>  
                                    <form action="{{ route('search_inventory') }}" method="post" class="form-horizontal" >
                                    {{ csrf_field() }} 
                                        <td> <label class="control-label"> Select Category </label> </td> 

                                        <td> 
                                    
                                            <select class="chzn -select category_id" required data-required="1" style="overflow:visible; width=100%" name="category_id" > 
            
                                                <option value="">Select Category...</option> 
                
                                                @foreach($categories as $category) 
                
                                                    <option value="{{ $category->id }}" >
                
                                                        {{ $category->name}} 
                
                                                    </option> 
                
                                                @endforeach 
                
                                            </select> 
                                        </td> 
                                        <td></td>

                                        <td><button class="btn btn-success " > <b>GO</b></button></td>
                                    </form>
                                    </tr> 
                                </div>
                                <div class="div_price">
                                    
                                    <tr>  
                                    <form action="{{ route('search_inventory') }}" method="post" class="form-horizontal" >
                                    {{ csrf_field() }} 
                                        <td> <label class="control-label"> Search By Price </label> </td> 
                                        
                                        <td> 

                                            <input type="text" name="price_from" placeholder="From" onfocus="this.type='number';" required data-required="1" class=" m-wrap"  style="width=100%"  min=0 oninput="validity.valid||(value=\\);"/> 

                                        </td> 

                                        <td> 

                                            <input type="text" name="price_to" placeholder="To" onfocus="this.type='number';" required data-required="1" class=" m-wrap"  style="width=100%"  min=1 oninput="validity.valid||(value=\\);"/> 

                                        </td>

                                        <td><button class="btn btn-success " > <b>GO</b></button></td>

                                    </form>

                                </tr> 

                                </div>
                                <div class="div_cost">
                                    
                                    <tr>  
                                    <form action="{{ route('search_inventory') }}" method="post" class="form-horizontal" >
                                    {{ csrf_field() }} 
                                        <td> <label class="control-label"> Search By Cost </label> </td> 
                                        
                                        <td> 

                                            <input type="text" name="cost_from" placeholder="From" onfocus="this.type='number';" required data-required="1" class=" m-wrap"  style="width=100%"  min=0 oninput="validity.valid||(value=\\);"/> 

                                        </td> 

                                        <td> 

                                            <input type="text" name="cost_to" placeholder="To" onfocus="this.type='number';" required data-required="1" class=" m-wrap"  style="width=100%"  min=1 oninput="validity.valid||(value=\\);"/> 

                                        </td>

                                        <td><button class="btn btn-success " > <b>GO</b></button></td>

                                    </form>

                                </tr> 

                                </div>                                

                            


                            </tbody>
                                            

                        </table>

                    </div>

                </div>

            </div>
        </fieldset>
    </div>
    @include('includes.items_table')
</div>
    
@endsection