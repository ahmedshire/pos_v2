@extends('layouts.app')
@php

    if(!session()->has('data')){
        $order = new \App\Order();
    }else{
        $data = session()->pull('data');
        $order = $data['order'];
        $orderItems = $data['orderItems'];
    }
    $sale = new \App\Sale();
    $title = 'Sale';
    $items =\App\Item::availableItems();
    $serial = $order->generateOrderSerial($sale);
    $personTitle = 'Customer';
    $order_type = 1;
    $action = "new_purchase";
@endphp
@section('content')
@include('includes.order_form')
@endsection