@extends('layouts.app')
@php
    if(!session()->has('data')){
        $order = new \App\Order();
    }else{
        $data = session()->pull('data');
        $order = $data['order'];
        $orderItems = $data['orderItems'];
    }

    $purchase = new \App\Purchase();
    $title = 'Purchase';
    $items =\App\Item::all();
    $serial = $order->generateOrderSerial($purchase);
    $personTitle = 'Supplier';
    $order_type = 0;
    $action = "new_purchase";
    
@endphp
@section('content')
@include('includes.order_form')
@endsection