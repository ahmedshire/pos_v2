@extends('layouts.app')
@php
    $refund = new \App\Refund();
    $order = new \App\Order();
    $title = 'Refund';
    $items =\App\Item::soldItems();
    $serial = $order->generateOrderSerial($refund);
    $personTitle = 'Customer';
    $order_type = 2;
    $action = "new_purchase";

@endphp
@section('content')
@include('includes.order_form')
@endsection