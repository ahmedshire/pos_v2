
<div class="row-fluid">

    <section id = "feedback">

    @include('layouts.feedback')

    </section>

    <!-- block -->

    <div class="block">

        <div class="navbar navbar-inner block-header">

            <div class="muted pull-left"><h4>{{$title}}</h4></div>
            <div class="btn-group pull-right">
                <a href="{{route('categories')}} "><button class="btn btn-success">Categories</button></a>
            </div>
        </div>

        <div class="block-content collapse in">

            <div class="span12">

                <!-- BEGIN FORM-->
<!--  -->
                <form action="" method="post" id="form_sample" class="form-horizontal">

                {{ csrf_field() }}

                    <fieldset>

                        <div class="alert alert-error hide">

                            <button class="close" data-dismiss="alert"></button>

                            You have some form errors. Please check below.

                        </div>

                        <div class="alert alert-success hide">

                            <button class="close" data-dismiss="alert"></button>

                            Your form validation is successful!

                        </div>


                        <div class="control-group">

                            <label class="control-label">Name<span class="required">*</span></label>

                            <div class="controls">

                                <input type="text" id="name" name="name" required data-required="1" class="span10 m-wrap" value="{{ $item->name ? $item->name : old('name') }}"/>

                            </div>

                        </div>


                        <div class="control-group">

                        <label class="control-label" >Category <span class="required">*</span></label>

                            <div class="controls">

                                <select class="chzn-select span10 m-wrap" required data-required="1" name="category_id" > 

                                   
                                    <option value="">Select Category...</option> 
                                   
                                    @foreach($categories as $category) 
                                   
                                        <option value="{{ $category->id }}" 
                                        {{ $category->id == ($item->category_id ? $item->category_id : old('category_id')) ?  'selected' : '' }}> 
                                   
                                            {{$category->name}} 
                                   
                                        </option> 
                                   
                                    @endforeach 

                                </select> 

                            </div>
                        </div>


                        <div class="control-group">

                            <label class="control-label" for="sellingPrice">Selling Price</label>

                            <div class="controls">
                            <input type="number" name="sellingPrice" id="sellingPrice"  value="{{$item->sellingPrice ? $item->sellingPrice : old('sellingPrice') }}"  data-required="1" class="span10 m-wrap" min="0" oninput="validity.valid||(value=\\);"/> 
                            </div>

                        </div>

                        <div class="control-group">

                            <label class="control-label" for="description">Description</label>

                            <div class="controls">

                            <textarea class="span10 m-wrap" name="description" placeholder="Enter text ..." >{{$item->description ? $item->description : old('description') }}</textarea>

                            </div>

                        </div>



                        <div class="form-actions">

                            <button type="submit" class="btn btn-primary">Save</button>

                            <button type="reset"  class="btn">Cancel</button>

                        </div>

                    </fieldset>



                </form>

                <!-- END FORM-->

            </div>

        </div>

    </div>

<!-- /block -->

</div>





<script>

    $(function(){
        $(".chzn-select").chosen();
    }

</script>