
<!-- block -->
<div class="block">
    <section id = "feedback">

    @include('layouts.feedback')

    </section>

    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">Inventory </div>
    </div>
    <div class="block-content collapse in">
        <div class="span12">
            <div class="table-toolbar">
                @if(!isset($is_report))
                <div class="btn-group">
                    <a href="{{route('new_item')}}"><button class="btn btn-success">Add New Item<i class="icon-plus icon-white"></i></button></a>
                </div>
                <div class="btn-group pull-right">
                    <a href="{{route('categories')}} "><button class="btn btn-success">Categories</button></a>
                </div>
                @else
                <div class="btn-group pull-right">
                    <a href="{{route('export_inventory_report')}} "><button class="btn btn-success"> <i class="icon-download-alt icon-white"></i>  Export CSV</button></a>
                </div>
                @endif
            </div>
                        
            <br/>
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example2">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Total Cost</th>
                        <th>Sold Quantity</th>
                        <th>In Stock</th>
                        <th>Profit</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($items as $index => $item)
                    <tr class="">
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->category->name }}</td>
                        <td>{{ $item->getTotalPurchaseCost() }}</td>
                        <td>{{ $item->quantityActualSold() }}</td>
                        <td>{{ $item->inStock() }}</td>
                        <td class="center"> {{ $item->profit() }}</td>

                        <td class="center">
                                                 
                            <form method="post" action="{{ route('delete_item',$item->id) }}" onsubmit="return confirm('This operation is irriversable. are you sure you want to continue ?');">
                            @if(isset($is_report))
                            <a href="{{route('single_item_report',$item->id) }}" class="btn btn-success btn-mini"><i class=" icon-info-sign icon-white"></i> Details</a>
                            @else
                            <a href="{{route('edit_item',$item->id) }}" class="btn btn-primary btn-mini"><i class="icon-edit icon-white"></i> Edit</a>
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-danger btn-mini"><i class="icon-remove icon-white"></i> Delete</button>
                            </form>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- /block -->

