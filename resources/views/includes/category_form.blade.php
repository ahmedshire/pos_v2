<div class="row-fluid">

    <section id = "feedback">

    @include('layouts.feedback')

    </section>

    <!-- block -->

    <div class="block">

        <div class="navbar navbar-inner block-header">

            <div class="muted pull-left"><h4> {{ $pageTitle }}</h4></div>

        </div>

        <div class="block-content collapse in">

            <div class="span12">

                <!-- BEGIN FORM-->
<!--  -->
                <form action="" method="post" id="form_sampl" class="form-horizontal">

                 {{csrf_field() }}

                    <fieldset>

                        <div class="alert alert-error hide">

                            <button class="close" data-dismiss="alert"></button>

                            You have some form errors. Please check below.

                        </div>

                        <div class="alert alert-success hide">

                            <button class="close" data-dismiss="alert"></button>

                            Your form validation is successful!

                        </div>


                        <div class="control-group">

                            <label class="control-label">Name<span class="required">*</span></label>

                            <div class="controls">

                                <input type="text" id="name" name="name" required data-required="1" class="span10 m-wrap" value="{{ $category->name ? $category->name : old('name') }}"/>

                            </div>

                        </div>


                        <div class="control-group">

                            <label class="control-label" for="description">Description</label>

                            <div class="controls">

                            <textarea class="span10 m-wrap" name="description" placeholder="Enter text ..." >{{$category->description ? $category->description : old('description')}} </textarea>

                            </div>

                        </div>



                        <div class="form-actions">

                            <button type="submit" class="btn btn-primary">Save</button>

                            <button type="reset"  class="btn">Cancel</button>

                        </div>

                    </fieldset>



                </form>

                <!-- END FORM-->

            </div>

        </div>

    </div>

<!-- /block -->

</div>


<!-- block -->
<div class="block">
    <section id = "feedback">

    @include('layouts.feedback')

    </section>

    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left"><h4>Categories</h4> </div>
    </div>
    <div class="block-content collapse in">
        <div class="span12">
            <div class="table-toolbar">
            </div>
            <br/>
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example2">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($categories as $index => $category)
                    <tr class="">
                        <td>{{ $index + 1 }} </td>
                        <td>{{ $category->name }} </td>
                        <td>{{ $category->description }} </td>
                        <td>{{ $category->items->count() }} </td>
                        <td class="center">
                            <form method="post" action="{{ route('delete_category',$category->id) }}" onsubmit="return confirm('You are about to delete the order and it\'s items ,This operation is irriversable. are you sure you want to continue ?');">
                            <a href="{{route('edit_category',$category->id) }}" class="btn btn-primary btn-mini"><i class="icon-edit icon-white"></i> Edit</a>
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-danger btn-mini"><i class="icon-remove icon-white"></i> Delete</button>
                                
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- /block -->