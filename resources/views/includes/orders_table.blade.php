<div class="row-fluid">

<section id = "feedback">
    @include('layouts.feedback')
</section>

    <!-- block -->
    <div class="block">

        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">{{ $title }} Orders </div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <div class="table-toolbar">
                @if(!isset($is_report))
                    <div class="btn-group">
                        <a href="{{ route($new_route) }}"><button class="btn btn-success">Add New <i class="icon-plus icon-white"></i></button></a>
                    </div>
                @else
                    <div class="btn-group pull-right">
                        <a href="{{route($export_route)}} "><button class="btn btn-success"> <i class="icon-download-alt icon-white"></i>  Export CSV</button></a>
                    </div>
                @endif
                </div>
                <br/>
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example2">
                    <thead>
                        <tr>
                            <th#</th>
                            <th>Order No#</th>
                            <th>{{$personTitle}}</th>
                            <th>Employee</th>
                            <th>Total Amount</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $index => $order)
                        <tr class="">
                            <td>{{ $order->relation() ? $order->relation()->serial : '' }}</td>
                            <td>{{ $order->person }}</td>
                            <td>{{ $order->employee }}</td>
                            <td class="center"> {{ $order->amount() }}</td>
                            <td class="center">
                            @if(isset($is_report))
                                <a href="{{route('single_order_export',$order->id) }}" class="btn btn-success btn-mini"><i class=" icon-info-sign icon-white"></i> Details</a>
                            @else
                                <form id="deleteOrder" method="post" action="{{ route($delete_route, $order) }}" onsubmit="return confirm('You are about to delete the order and it\'s items ,This operation is irriversable. are you sure you want to continue ?');">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="btn btn-danger btn-mini"><i class="icon-remove icon-white"></i> Delete</button>
                                </form>
                            @endif
                            </td>
                    
                        </tr>
                        
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /block -->
</div>
<script>
$('[data-toggle=confirmation]').confirmation({
  rootSelector: '[data-toggle=confirmation]',
  // other options
});
</script>