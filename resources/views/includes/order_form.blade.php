
@extends('layouts.app')


@section('content')

<div class="row-fluid">

<section id = "feedback">
    @include('layouts.feedback')
</section>

    <!-- block -->

    <div class="block">

        <div class="navbar navbar-inner block-header">

            <div class="muted pull-left">New {{$title}} Order# <b>{{ $serial }}</b></div>

        </div>

        <div class="block-content collapse in">

            <div class="span12">

                <!-- BEGIN FORM-->
<!--  -->
                <form action="{{ route($action) }}" method="post" id="form_sampl_1" class="form-horizontal">

                {{ csrf_field() }}

                    <fieldset>

                        <div class="alert alert-error hide">

                            <button class="close" data-dismiss="alert"></button>

                            You have some form errors. Please check below.

                        </div>

                        <div class="alert alert-success hide">

                            <button class="close" data-dismiss="alert"></button>

                            Your form validation is successful!

                        </div>

                        <input name="serial" type="hidden" value="{{ $serial }}">

                        <input type="hidden" name="order_type" id="order_type" value="{{$order_type}}" />

                        <div class="control-group">

                            <label class="control-label">{{$personTitle}}<span class="required">*</span></label>

                            <div class="controls">

                                <input type="text" id="person" name="person" required data-required="1" class="span10 m-wrap" value="{{ old('person') }}"/>

                            </div>

                        </div>

                        <div class="control-group">

                            <label class="control-label">Employee<span class="required">*</span></label>

                            <div class="controls">

                                <input type="text" name="employee" required data-required="1" class="span10 m-wrap" value="{{ old('employee') }}"/>

                            </div>

                        </div>



                        <div class="control-group">

                            <label class="control-label" >Date</label>

                            <div class="controls">

                            <input type="text" readOnly class="span10 m-wrap datepicker"  id="created_at" value="{{ old('date') ? old('person')  :date('m/d/Y') }}">

                            </div>

                        </div>

                        <div class="control-group">

                            <label class="control-label" for="description">Description</label>

                            <div class="controls">

                            <textarea class="span10 m-wrap" name="description" placeholder="Enter text ..." >{{ old('description') }}</textarea>

                            </div>

                        </div>



                        <fieldset>

                            <div class="block">

                            <div class="navbar navbar-inner block-header">

                                <div class="muted pull-left">Add Items</div>

                                <div class="muted pull-right">

                                    <a id="add" class="btn btn-success btn-mini" class="top-bar-right"> <b>+</b></a>

                                </div>

                            </div>

                            <div class="block-content collapse in">

                                <div class="span12">

  									<table class="table span12">

						              <thead>

						                <tr>

                                            <th>#</th>

                                            <td>Choose an Item  <span class="required">*</span></td>

                                            

                                            <td>Quantity <span class="required">*</span></td>

                                            @if(!isset($purchase))

                                            <td>{{ isset($sale) ? "Qty in Stock" : "Qty Sold" }}</td>

                                            @endif
                                            
                                            <td>Price <span class="required">*</span></td>

                                            @if(!isset($purchase)) 



                                            <td>{{ isset($sale) ? "Purchase Price" : "Sale Price" }}</td>



                                            @endif
                                            <td>Amount <span class="required">*</span></td>

						                </tr>

						              </thead>

                                      <tbody class="detail">

                                      @if(isset($orderItems))

                                        @foreach($orderItems as $index => $orderItem)

                                        <tr>  

                                            <td> {{ $index + 1 }} </td> 

                                            <td> 

                                                <select class="chzn-select item_id" data-required="3" style="overflow:visible; width=100%" name="item_id[]" > 

                                                    <option value="">Select Item...</option> 

                                                    @foreach($items as $item) 

                                                        <option value="{{ $item->id }}" 

                                                            data-id="{{$order_type == 1 ? $item->inStock() : $item->quantityActualSold()}}" 

                                                            data-name="{{$order_type == 1 ? $item->getAvgCost() : $item->getAvgPrice()  }}"

                                                            {{ $item->id == $orderItem->item_id ?  'selected' : '' }}

                                                             > 

                                                            {{$item->name}} 

                                                        </option> 

                                                    @endforeach 

                                                </select> 

                                            </td> 

                        

                                           <td> 

                                                <input type="text" name="quantity[]" id="quantity" required data-required="1" value="{{ $orderItem->quantity }}" class="input-small m-wrap quantity" step="any" min=0 oninput="validity.valid||(value=\\);"/> 

                                            </td> 

                                            @if($order_type != 0)

                                            <td> 

                                                <input type="text" name="quantity_in_db" id="quantity_in_db" value="{{$order_type == 1 ? $item->inStock() : $item->quantityActualSold()}}" disabled data-required="1" class="input-small m-wrap quantity_in_db"/> 

                                            </td> 

                                            @endif
                                            <td> 

                                                <input type="text" name="price[]" id="price" value="{{$orderItem->price}}" required data-required="1" class="input-small m-wrap price" step="any" min=0 /> 

                                            </td> 

                                            @if($order_type != 0)

                                            <td> 

                                                <input type="text" name="price_in_db" id="price_in_db" value="{{$order_type == 1 ? $item->getAvgCost() : $item->getAvgPrice()  }}" disabled data-required="1" class="input-small m-wrap price_in_db" /> 

                                            </td>

                                            @endif
                                           <td> 

                                                <input type="text" id="amount[]" name="amount" data-required="1" value="{{ $orderItem->getAmount() }}" class="input-small m-wrap amount" value="0" />  

                                            </td> 

                                            <td><a class="btn remove btn-danger btn-mini"> <b>x</b></a></td>

                                        </tr> 

                                        @endforeach 

                                        @else

                                      <script>

                                      $(function(){

                                         addnewrow();

                                      });

                                      </script>



                                        @endif 

						              </tbody>

                                        <tfoot>

                                            <tr>

                                                <th colspan="6"></th>  

                                                <th style="text-align:center;" ><b>Total : <p class="total"> 0 </p></b></th>  

                                                <th>    



                                                </th>                                                    

                                            </tr>

                                        </tfoot>                                      

						            </table>

                                </div>

                            </div>

                        </div>



                        </fieldset>



                        <div class="form-actions">

                            <button type="submit" class="btn btn-primary">Save</button>

                            <button type="reset"  class="btn">Cancel</button>

                        </div>

                    </fieldset>



                </form>

                <!-- END FORM-->

            </div>

        </div>

    </div>

<!-- /block -->

</div>





<script>

    $(function(){

        $('#add').click(function(){  

            addnewrow();

        }); 



        $('body').delegate('.price, .quantity, .item_id','change',function(){

            var tr = $(this).parent().parent();  

            var price = parseFloat(tr.find('.price').val());  

            var qty = parseFloat(tr.find('.quantity').val()); 

            var order_type = $('#order_type').val();

            if(order_type != 0){

                var item_select = tr.find('.item_id'); 

                var quantity_in_db = parseFloat(item_select.find(':selected').data('id'));

                var price_in_db = parseFloat(item_select.find(':selected').data('name'));



                if(order_type == '1'){

                    if (price < price_in_db) {

                        price = 0;

                        alert('Selling price Can not be less than the purchase cost')

                        tr.find('.price').val('');  

                        // $('#price').focus();  

                    }

                    if (qty > quantity_in_db) {

                        quantity = 0;

                        alert('Quantity to sell can not be greated than quantity in stock')

                        tr.find('.quantity').val('');  

                    }

                }else if(order_type == '2'){

                    if (price > price_in_db) {

                        price = 0;

                        alert('Refund amount Can not be greater than selling price')

                        tr.find('.price').val('');  

                    }

                    if (qty > quantity_in_db) {

                        quantity = 0;

                        alert('Returned quantity can not be greated than quantity sold')

                        tr.find('.quantity').val('');  

                    }                    

                }

            }

            var amount = qty * price;

            tr.find('.amount').val(parseFloat(amount));  

            total();

        });

        $('body').delegate('.item_id','change',function(){  



            var tr = $(this).parent().parent();  

            var item_select = tr.find('.item_id');

            if(order_type != 0){

                var quantity_in_db = item_select.find(':selected').data('id');

                var price_in_db = item_select.find(':selected').data('name');

                tr.find('.price_in_db').val(price_in_db); 

                tr.find('.quantity_in_db').val(quantity_in_db); 

            }



            // var val = $(this).val(); // get the value of the current changed select

            // var i = $(this).index(); // get it's index (position in the group starting from 0)

            // $('select:not(:eq('+i+'))').each(function () { // loop through each select 

            //     if ($(this).val() === val && !($(this).val() === "")) { // check if the value of the next select 

            //                                     // is the same as the changed one

            //         alert('You can not add the same item more than once in a single order' + $(this).val()); 



            //         // alert the ones with the same value 

            //     }

            // });                



        });

        $('body').delegate('.remove','click',function(){  

            $(this).parent().parent().remove();

            total();

        });


        $('body').delegate('.amount','change',function(){  

            
            var tr = $(this).parent().parent();  


            
            var amount = tr.find('.amount').val();
            
            var quantity = tr.find('.quantity').val();
            
            var inputPrice = tr.find('.price');
            
            inputPrice.val(parseFloat(amount) / parseFloat(quantity));

            total();
            
        });

    });





    function total(){  

        var t=0;  

        $('.amount').each(function(i,e){  

            var amount =$(this).val()-0;  

            t+=amount;  

        });  

        $('.total').html(t);  

    }  


    function addnewrow()   

    {  
        /**
        *
        *   This Method is to dynamically add row of elements to the table detail
        *   it will check the order type first -> then it will which elements to hide 
        *   also in item select; the data will be different if the order type is different
        *
        * */
        var order_type = document.getElementById('order_type').value;

        var n=($('.detail tr').length + 1);  

        var tr = '<tr>'+  

                '<td>'+ n++ +'</td>' +

                '<td>' +

                    '<select class="chzn-select item_id" data-required="1" required style="overflow:visible; width=100%" name="item_id[]" >' +

                        '<option value="">Select Item...</option>' +

                        '@foreach($items as $item)' +

                            '<option value="{{ $item->id }}"' +

                                'data-id="{{$order_type == 1 ? $item->inStock() : $item->quantityActualSold()}}"' +

                                'data-name="{{$order_type == 1 ? $item->getAvgCost() : $item->getAvgPrice()  }}" >' +

                                '{{$item->name}}' +

                                '</option>' +

                        '@endforeach' +

                    '</select>' +

                '</td>';

                tr +='<td>' +

                    '<input type="text" name="quantity[]" id="quantity" required data-required="1" class="input-small m-wrap quantity"/>' +

                '</td>' ;

                if(order_type != 0){

                tr = tr+  '<td>' +

                    '<input type="text" name="quantity_in_db" id="quantity_in_db" disabled data-required="1" step="any" class="input-small m-wrap quantity_in_db"/>' +

                '</td>' ;

                }
                tr += '<td>' +

                    '<input type="text" name="price[]" id="price" required data-required="1" class="input-small m-wrap price"/>' +

                '</td>' ;

                if(order_type != 0){

                tr += '<td>' +

                    '<input type="text" name="price_in_db" id="price_in_db" disabled data-required="1" class="input-small m-wrap price_in_db" />' +

                '</td>';

                }

                tr +='<td>' +

                    '<input type="text" id="amount[]" name="amount"  data-required="1" class="input-small m-wrap amount" value="0" /> ' +

                '</td>' +

                '<td><a class="btn remove btn-danger btn-mini"> <b>x</b></a></td>' +

            '</tr>';  

        $('.detail').append(tr);  

        $(".chzn-select").chosen();

    }

</script>

@endsection



