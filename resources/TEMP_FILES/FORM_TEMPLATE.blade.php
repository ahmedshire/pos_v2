<!DOCTYPE html>
<html>
    
    <head>
        <title>Forms</title>
        <!-- Bootstrap -->
        <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">
        <link href="{{ asset('bootstrap/css/bootstrap-responsive.min.css') }}" rel="stylesheet" media="screen">
        <link href="{{ asset('vendors/easypiechart/jquery.easy-pie-chart.css') }}" rel="stylesheet" media="screen">
        <link href="{{ asset('assets/styles.css') }}" rel="stylesheet" media="screen">
        <link href="{{ asset('assets/DT_bootstrap.css') }}" rel="stylesheet" media="screen">
        <!-- <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"> -->
        <!-- <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen"> -->
        <!-- <link href="assets/styles.css" rel="stylesheet" media="screen"> -->
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="vendors/flot/excanvas.min.js"></script><![endif]-->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="{{ asset('vendors/modernizr-2.6.2-respond-1.1.0.min.js') }}"></script>
        <!-- <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script> -->
    </head>
    
    <body>
         <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Form Validation</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
					<!-- BEGIN FORM-->
					<form action="" method="post" id="form_sample_1" class="form-horizontal">
						<fieldset>
							<div class="alert alert-error hide">
								<button class="close" data-dismiss="alert"></button>
								You have some form errors. Please check below.
							</div>
							<div class="alert alert-success hide">
								<button class="close" data-dismiss="alert"></button>
								Your form validation is successful!
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">Date input</label>
                                <div class="controls">
                                <input type="text" class="span6 m-wrap datepicker" id="date01" value="02/16/12">
                                <p class="help-block">In addition to freeform text, any HTML5 text-based input appears like so.</p>
                                </div>
                            </div>
  							<div class="control-group">
  								<label class="control-label">Name<span class="required">*</span></label>
  								<div class="controls">
  									<input type="text" name="name" data-required="1" class="span2 m-wrap"/>
  									 MAGACA <input type="text" name="name" data-required="1" class="span2 m-wrap"/>
  									NAMAYE <input type="text" name="name" data-required="1" class="span2 m-wrap"/>
  								</div>
  							</div>
  							<div class="control-group">
  								<label class="control-label">Email<span class="required">*</span></label>
  								<div class="controls">
  									<input name="email" type="text" class="span6 m-wrap"/>
  								</div>
  							</div>
  							<div class="control-group">
  								<label class="control-label">URL<span class="required">*</span></label>
  								<div class="controls">
  									<input name="url" type="text" class="span6 m-wrap"/>
  									<span class="help-block">e.g: http://www.demo.com or http://demo.com</span>
  								</div>
  							</div>
  							<div class="control-group">
  								<label class="control-label">Number<span class="required">*</span></label>
  								<div class="controls">
  									<input name="number" type="text" class="span6 m-wrap"/>
  								</div>
  							</div>
  							<div class="control-group">
  								<label class="control-label">Digits<span class="required">*</span></label>
  								<div class="controls">
  									<input name="digits" type="text" class="span6 m-wrap"/>
  								</div>
  							</div>
  							<div class="control-group">
  								<label class="control-label">Credit Card<span class="required">*</span></label>
  								<div class="controls">
  									<input name="creditcard" type="text" class="span6 m-wrap"/>
  									<span class="help-block">e.g: 5500 0000 0000 0004</span>
  								</div>
  							</div>
  							<div class="control-group">
  								<label class="control-label">Occupation&nbsp;&nbsp;</label>
  								<div class="controls">
  									<input name="occupation" type="text" class="span6 m-wrap"/>
  									<span class="help-block">optional field</span>
  								</div>
  							</div>
  							<div class="control-group">
  								<label class="control-label">Category<span class="required">*</span></label>
  								<div class="controls">
  									<select class="span6 m-wrap chzn-select" name="category">
  										<option value="">Select...</option>
  										<option value="Category 1">Category 1</option>
  										<option value="Category 2">Category 2</option>
  										<option value="Category 3">Category 5</option>
  										<option value="Category 4">Category 4</option>
  									</select>
  								</div>
  							</div>
  							<div class="form-actions">
  								<button type="submit" class="btn btn-primary">Validate</button>
  								<button type="button" class="btn">Cancel</button>
  							</div>
						</fieldset>
					</form>
					<!-- END FORM-->
				</div>
			    </div>
			</div>
                     	<!-- /block -->
		    </div>
                     <!-- /validation -->
        <link href="{{ asset('vendors/datepicker.css') }}" rel="stylesheet" media="screen">
        <link href="{{ asset('vendors/uniform.default.css') }}" rel="stylesheet" media="screen">
        <link href="{{ asset('vendors/chosen.min.css') }}" rel="stylesheet" media="screen">

        <link href="{{ asset('vendors/wysiwyg/bootstrap-wysihtml5.css') }}" rel="stylesheet" media="screen">

        <script src="{{ asset('vendors/jquery-1.9.1.js') }}"></script>
        <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('vendors/jquery.uniform.min.js') }}"></script>
        <script src="{{ asset('vendors/chosen.jquery.min.js') }}"></script>
        <script src="{{ asset('vendors/bootstrap-datepicker.js') }}"></script>
        <script src="{{ asset('vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('assets/form-validation.js') }}"></script>
        <script src="{{ asset('assets/scripts.js') }}"></script>
        <script>

	jQuery(document).ready(function() {   
	   FormValidation.init();
	});
    $(function() {
        $(".datepicker").datepicker();
        $(".chzn-select").chosen();
    });
    </script>
</body>

</html>